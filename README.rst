========
 pdfocr
========

pdfocr adds an OCR text layer to scanned PDF files, allowing them to be searched. It currently depends on Ruby 1.8.7 or above, and uses ocropus, cuneiform, or tesseract for performing OCR.

To use, run::

  pdfocr -i input.pdf -o output.pdf

For more details, see the manpage.

Credits
=======

pdfocr was written by Geza Kovacs

The original pdfocr is hosted at http://github.com/gkovacs/pdfocr

Christian Pietsch added tesseract support for this fork of pdfocr.

Brandon Nielsen changed this fork to use `Poppler`_ in place of `pdftk`_ and `hocr-tools` in place of `ExactImage`.

.. _Poppler: http://poppler.freedesktop.org/
.. _pdftk: https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/
.. _hocr-tools: https://github.com/tmbdev/hocr-tools
.. _ExactImage: https://exactcode.com/opensource/exactimage/
